# 這些只是為了減少路徑長度... 嗎?
include_directories(${CMAKE_SOURCE_DIR}/include/)
include_directories(${CMAKE_SOURCE_DIR}/vendor/glfw/include/)
include_directories(${CMAKE_SOURCE_DIR}/vendor/glm/)
include_directories(${CMAKE_SOURCE_DIR}/vendor/imgui/)
include_directories(${CMAKE_SOURCE_DIR}/vendor/imgui/backends/)
include_directories(${VULKAN_SDK}/include/)

link_directories(${CMAKE_SOURCE_DIR}/vendor/glfw/lib/)
link_directories(${VULKAN_SDK}/lib/)

set(CMAKE_EXE_LINKER_FLAGS /NODEFAULTLIB:msvcrt.lib)

# 這些是一起編譯的檔案
file(GLOB source_files CONFIGURE_DEPENDS
  "${PROJECT_SOURCE_DIR}/src/*.cpp"
  "${PROJECT_SOURCE_DIR}/include/*.h"
  # imgui
  "${PROJECT_SOURCE_DIR}/vendor/imgui/*.cpp"
  "${PROJECT_SOURCE_DIR}/vendor/imgui/*.h"
  "${PROJECT_SOURCE_DIR}/vendor/imgui/backends/imgui_impl_glfw.cpp"
  "${PROJECT_SOURCE_DIR}/vendor/imgui/backends/imgui_impl_glfw.h"
  "${PROJECT_SOURCE_DIR}/vendor/imgui/backends/imgui_impl_opengl2.cpp"
  "${PROJECT_SOURCE_DIR}/vendor/imgui/backends/imgui_impl_opengl2.h"
  "${PROJECT_SOURCE_DIR}/vendor/imgui/backends/imgui_impl_vulkan.cpp"
  "${PROJECT_SOURCE_DIR}/vendor/imgui/backends/imgui_impl_vulkan.h"
)

add_executable(${PROJECT_NAME} ${source_files})

set_property(TARGET ${PROJECT_NAME} PROPERTY CXX_STANDARD 17)

target_link_libraries(${PROJECT_NAME}
  glfw3.lib
  glfw3_mt.lib
  glfw3dll.lib
  opengl32.lib  # FIX: https://discourse.glfw.org/t/1-unresolved-externals/1847/2
  vulkan-1.lib)

# Copy resources
file(COPY ${CMAKE_SOURCE_DIR}/vendor/glfw/lib/glfw3.dll DESTINATION Release)
file(COPY ${CMAKE_SOURCE_DIR}/vendor/glfw/lib/glfw3.dll DESTINATION Debug)
