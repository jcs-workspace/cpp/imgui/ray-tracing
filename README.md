# ray-tracing

Only includes minial components to start a [ImGui][] project.

## 🔨 How to use?

```sh
# clone this repo
git clone https://gitlab.com/jcs-workspace/minimal-imgui --recurse

# navigate to project dir
cd minimal-imgui

# Build CMake project
start scripts/build.bat
```

<!-- Links -->

[ImGui]: https://github.com/ocornut/imgui
